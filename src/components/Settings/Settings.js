import React from "react";
import { Link } from "react-router-dom";
import Switch from "../Switch";

const Settings = ({
  synchronizedProjects,
  lastUpdateTime,
  currentPage,
  accessToken,
  toggleProjectEdition,
  loadMoreProjects
}) => (
  <div>
    <p>Settings</p>
    <p>{lastUpdateTime}</p>
    {synchronizedProjects && (
      <ul>
        {synchronizedProjects.map(project => (
          <li key={project.id}>
            <p>
              {project.name} {project.enabled ? `[enabled]` : `[disabled]`}
            </p>
            <Switch
              id={project.id}
              enabled={project.enabled}
              toggleProjectEdition={toggleProjectEdition}
            />
          </li>
        ))}
      </ul>
    )}
    <div>
      <button onClick={() => loadMoreProjects(accessToken, currentPage + 1)}>
        Load More
      </button>
    </div>
    <Link to="/">Go Home</Link>
  </div>
);

export default Settings;
